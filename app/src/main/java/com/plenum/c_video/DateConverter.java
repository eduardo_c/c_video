package com.plenum.c_video;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jcastro on 17/07/2016.
 */
public class DateConverter {

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", java.util.Locale.getDefault());

    /**
     * Metodo para parsear una clase Date a String
     * @param date la cual sera parseada
     * @return Devuelve la fecha en String
     */
    public String DateToString(Date date) {
        return sdf.format(date);
    }

    /**
     * Metodo para parsear un String a un Date
     * @param string la cual sera parseada
     * @return Devuelve la fecha en formato fecha
     */
    public Date StringToDate(String string) throws ParseException {
        return sdf.parse(string);
    }
}
