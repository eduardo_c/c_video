package com.plenum.c_video;

/**
 * Created by jcastro on 18/07/2016.
 */
public class PermissionRequestCodes {

    public static final int ACCESS_FINE_LOCATION = 1;
    public static final int ACCESS_FINE_LOCATION_FINISH = 2;
    public static final int CAMERA = 3;
    public static final int RECORD_AUDIO = 4;
    public static final int CAMERA_PREPARE_RECORDER = 5;
    public static final int STORAGE = 6;
}
