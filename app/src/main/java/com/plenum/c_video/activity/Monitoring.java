package com.plenum.c_video.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.plenum.c_video.PermissionRequestCodes;
import com.plenum.c_video.R;
import com.plenum.c_video.auth.SessionManagement;
import com.plenum.c_video.controllers.MonitoringController;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.model.Recorrido;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Monitoring extends AppCompatActivity implements View.OnClickListener, SensorEventListener, SurfaceHolder.Callback{

    private final String LOG_TAG = Monitoring.class.getName();

    private SensorManager sensorManager;
    private float ax = 0.0f, ay = 0.0f, az = 0.0f;
    private float wx = 0.0f, wy = 0.0f, wz = 0.0f;
    private float fx = 0.0f, fy = 0.0f, fz = 0.0f;
    private double latitud, longitud, altitud;
    private long elapsedMillis = 0;
    private int level;
    private View mMonitoringView;
    private View mSensingView;
    LocationManager mlocManager;
    MyLocationListener mlocListener;
    public Chronometer cronometro;
    private boolean btnOn = true;
    private boolean isClickeable = false;
    private InsertRecorrido insertRecorrido;
    private UpdateRecorrido updateRecorrido;
    private RetrieveLastRecorrido retrieveLastRecorrido;
    private InsertLecturaDispositivo insertLecturaDispositivo;
    private DeleteSensado deleteSensado;
    private Button btnInicioRuta;
    private Vibrator myVibrator;
    int shortAnimTime = 15000;
    Timer insertLecturasDispositivoTimer;
    private Recorrido lastRecorrido;
    private TextView txvEstatusBoton;
    int request_code = 1;
    SessionManagement session;
    private int transportistaId;


    private Button recording_button;
    MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;

    /**
     * Método de la creacion de la UI
     * @param Bundle savedInstanceState
     * @return No devuelve ningún valor
     * @throws No dispara ninguna excepcion
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_monitoring);

        session = new SessionManagement(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        transportistaId = Integer.parseInt(user.get(SessionManagement.KEY_TRANSPORTISTA_ID));

        /**
         * Utilizacion de la clase LocationManager para obtener la localizacion GPS
         */
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener();
        mlocListener.setMainActivity(this);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermissionRequestCodes.ACCESS_FINE_LOCATION);
        }
        else
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) mlocListener);

        mMonitoringView = findViewById(R.id.monitoring);
        mSensingView = findViewById(R.id.monitoring_sensing);

        txvEstatusBoton = (TextView) findViewById(R.id.txvEstatusBoton);

        btnInicioRuta = (Button) findViewById(R.id.btnInicioRuta);
        btnInicioRuta.setOnClickListener(this);

        recording_button = (Button)findViewById(R.id.recording_button);
        recording_button.setOnClickListener(this);


        myVibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);

        cronometro = (Chronometer) findViewById(R.id.cronometro);
        cronometro.setText("00:00:00");
        cronometro.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                CharSequence text = chronometer.getText();
                if (text.length() == 4) {
                    cronometro.setText("00:0" + text);
                } else if (text.length() == 5)
                    cronometro.setText("00:" + text);
                else if (text.length() == 7) {
                    cronometro.setText("0" + text);
                }
            }
        });

        getSensors();
        showProgress(false);

        recorder = new MediaRecorder();
        recorder.setOrientationHint(90);
        initRecorder();
        SurfaceView cameraView = (SurfaceView) findViewById(R.id.surface_camera);
        holder = cameraView.getHolder();
        holder.addCallback(this);
        //Metodo obsoleto a partir de Android 3.0 Honeycomb
        //holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void initRecorder() {
        //recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_UPLINK);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECORD_AUDIO}, PermissionRequestCodes.RECORD_AUDIO);
        }
        else{
            recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, PermissionRequestCodes.CAMERA);
        }
        else{
            recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

            CamcorderProfile cpHigh = CamcorderProfile
                    .get(CamcorderProfile.QUALITY_LOW);
            recorder.setProfile(cpHigh);
            Calendar calendarFinal = Calendar.getInstance();

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH:ss:SS");
            String formattedDate = df.format(calendarFinal.getTime());
            recorder.setOutputFile(Environment.getExternalStorageDirectory().getPath() + "/video" + formattedDate + ".mp4");
        }
    }

    private void prepareRecorder() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PermissionRequestCodes.STORAGE);
        }
        else{
            recorder.setPreviewDisplay(holder.getSurface());
            try {
                recorder.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                finish();
            } catch (IOException e) {
                e.printStackTrace();
                finish();
            }
        }
    }
    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();
    }
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (recording) {
            recorder.stop();
            recording = false;
        }
        recorder.release();
        finish();
    }




    /**
     * Metodo que se ejecuta sobre el hilo de la UI sin interrumpir las ejecuciones de los demas metodos
     * @param No recibe ningun parametro
     * @return No devuelve ningun valor
     * @throws No devuelve ninguna excepcion
     */
    private Runnable Timer_Tick = new Runnable() {
        public void run() {
            insertLecturaDispositivo = new InsertLecturaDispositivo();
            insertLecturaDispositivo.execute();

        }
    };

    /**
     * Metodo que se ejecuta para meter ejecuciones directamente sobre el hilo de la UI sin interrumpir las ejecuciones de los demas metodos
     * @param No recibe ningun parametro
     * @return No devuelve ningun valor
     * @throws No devuelve ninguna excepcion
     */
    private void TimerMethod() {
        this.runOnUiThread(Timer_Tick);
    }

    /**
     * Metodo que se ejecuta para agregar las opciones del menu que seran desplegadas
     * @param Menu menu
     * @return Devuelve un boolean
     * @throws No devuelve ninguna excepcion
     */
    private int getBatteryLevel() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);
        level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        return level;
    }

    /**
     * Metodo que se ejecuta al inicializar la UI y se encarga del desvanecido entre el Layout de Calibrado y el Layout de Monitoreo
     * @param final boolean show
     * @return No devuelve ningun valor
     * @throws No devuelve ninguna excepcion
     */
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            insertRecorrido = new InsertRecorrido();
            insertRecorrido.execute();
            retrieveLastRecorrido = new RetrieveLastRecorrido();
            retrieveLastRecorrido.execute();
            btnOn = false;

            insertLecturasDispositivoTimer = new Timer();
            insertLecturasDispositivoTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    TimerMethod();
                }
            }, 0, 75); /* Cada 75 milesimas de segundo guarda una LecturaDispositivo */

            mSensingView.setVisibility(View.VISIBLE);
            mSensingView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSensingView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            mMonitoringView.setVisibility(View.VISIBLE);
            mMonitoringView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMonitoringView.setVisibility(show ? View.GONE : View.VISIBLE);
					/* Finaliza la animacion */
                    updateRecorrido = new UpdateRecorrido();
                    updateRecorrido.execute();
                    isClickeable = true;
                    btnOn = true;
                    insertLecturasDispositivoTimer.cancel();
                    //txvEstatusBoton.setText(getResources().getString(R.string.Monitoring_Start_Recorrido));
                }
            });
        } else {
            mSensingView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMonitoringView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Metodo que se ejecuta para agregar las opciones del menu que seran desplegadas
     * @param Menu menu
     * @return Devuelve un boolean
     * @throws No devuelve ninguna excepcion
     */
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.monitoring, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    /**
     * Metodo que se ejecuta al ser seleccionada una opcion desde el menu de opciones desplegado con el boton de Menu
     * @param MenuItem item
     * @return Devuelve un boolean
     * @throws No devuelve ninguna excepcion
     */
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
            case R.id.action_settings:
                Intent pickContactIntent = new Intent(this, SendingData.class);
                startActivityForResult(pickContactIntent, request_code);
        }
        return super.onOptionsItemSelected(item);
    }*/

    /**
     * Metodo que se ejecuta al obtener un resultado de la UI que se llamo a ejecutar desde esta UI
     * @param int requestCode, int resultCode, Intent data
     * @return No devuelve algun valor
     * @throws No devuelve ninguna excepcion
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(Monitoring.this, getResources().getString(R.string.Monitoring_On_Activity_Result), Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Metodo que se ejecuta al cambiar la precision de los sensores
     * @param Sensor sensor, int precision
     * @return No devuelve algun valor
     * @throws No devuelve ninguna excepcion
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int precision) {
        switch (precision) {
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
                break;
            case SensorManager.SENSOR_STATUS_UNRELIABLE:
                break;
        }
    }

    /**
     * Metodo que se ejecuta al cambiar el valor de cada sensor
     * @param SensorEvent evento
     * @return No devuelve algun valor
     * @throws No devuelve ninguna excepcion
     */
    @Override
    public void onSensorChanged(SensorEvent evento) {
        onAccuracyChanged(evento.sensor, evento.accuracy);
        switch (evento.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                ax = evento.values[0];
                ay = evento.values[1];
                az = evento.values[2];
                break;
            case Sensor.TYPE_GYROSCOPE:
                wx = evento.values[0];
                wy = evento.values[1];
                wz = evento.values[2];
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                fx = evento.values[0];
                fy = evento.values[1];
                fz = evento.values[2];
                break;
        }
    }

    /**
     * Metodo que se ejecuta para insertar un Recorrido en la base de datos del movil
     */
    public class InsertRecorrido extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Calendar c = new GregorianCalendar();

            Recorrido recorrido = new Recorrido();
            recorrido.setTransportistaId(transportistaId);
            recorrido.setFechaInicio(c.getTime());
            recorrido.setEnviado(false);

            MonitoringController controller = new MonitoringController();
            controller.insertRecorrido(Monitoring.this, recorrido);

            return null;
        }
    }

    /**
     * Metodo que se ejecuta para actualziar un Recorrido en la base de datos del movil
     */
    public class UpdateRecorrido extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Calendar c = new GregorianCalendar();
            MonitoringController controller = new MonitoringController();

            Recorrido recorrido = new Recorrido();
            recorrido = controller.retrieveLastRecorrido(Monitoring.this);
            recorrido.setFechaFin(c.getTime());

            controller.updateRecorrido(Monitoring.this, recorrido);

            return null;
        }
    }

    /**
     * Metodo que se ejecuta para consltar el ultimo un Recorrido en la base de datos del movil
     */
    public class RetrieveLastRecorrido extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... params) {
            MonitoringController controller = new MonitoringController();
            lastRecorrido = controller.retrieveLastRecorrido(Monitoring.this);
            return null;
        }
    }

    /**
     * Metodo que se ejecuta para insertar una LecturaDispositivo en la base de datos del movil
     */
    public class InsertLecturaDispositivo extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Calendar c = new GregorianCalendar();

            LecturaDispositivo lecturaDispositivo = new LecturaDispositivo();
            lecturaDispositivo.setTransportistaId(transportistaId);
            lecturaDispositivo.setRecorridoId(lastRecorrido.getRecorridoId());
            lecturaDispositivo.setFechaRegistro(c.getTime());
            lecturaDispositivo.setAcelerometroX(String.valueOf(ax));
            lecturaDispositivo.setAcelerometroY(String.valueOf(ay));
            lecturaDispositivo.setAcelerometroZ(String.valueOf(az));
            lecturaDispositivo.setMagnetometroX(String.valueOf(fx));
            lecturaDispositivo.setMagnetometroY(String.valueOf(fy));
            lecturaDispositivo.setMagnetometroZ(String.valueOf(fz));
            lecturaDispositivo.setGiroscopioX(String.valueOf(wx));
            lecturaDispositivo.setGiroscopioY(String.valueOf(wy));
            lecturaDispositivo.setGiroscopioZ(String.valueOf(wz));
            lecturaDispositivo.setLatitud(latitud);
            lecturaDispositivo.setLongitud(longitud);
            lecturaDispositivo.setAltitud(altitud);
            lecturaDispositivo.setNivelBateria(getBatteryLevel());

            MonitoringController controller = new MonitoringController();
            controller.insertLecturaDispositivo(Monitoring.this, lecturaDispositivo);

            return null;
        }
    }

    /**
     * Metodo que se ejecuta para borrar la informacion de un Recorrido asi como sus LecturasDispositivo asociadas en la base de datos del movil
     */
    public class DeleteSensado extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... params) {
            MonitoringController controller = new MonitoringController();
            controller.deleteRecorrido(Monitoring.this, lastRecorrido);

            return null;
        }
    }

    /**
     * Metodo que se ejecuta al dar click sobre un boton de la UI
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInicioRuta:
                if (btnOn && isClickeable) {

                    insertLecturasDispositivoTimer = new Timer();
                    insertLecturasDispositivoTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            TimerMethod();
                        }

                    }, 0, 75); /* Cada 75 milesimas de segundo guarda una LecturaDispositivo */

                    elapsedMillis = 0;
                    btnInicioRuta.setBackgroundResource(R.drawable.stop_big);
                    //txvEstatusBoton.setText(getResources().getString(R.string.Monitoring_Stop_Recorrido));
                    myVibrator.vibrate(100);
                    btnOn = false;
                    cronometro.setBase(SystemClock.elapsedRealtime());
                    cronometro.start();

                    insertRecorrido = new InsertRecorrido();
                    insertRecorrido.execute();
                    retrieveLastRecorrido = new RetrieveLastRecorrido();
                    retrieveLastRecorrido.execute();
                } else if (isClickeable) {
                    insertLecturasDispositivoTimer.cancel();
                    elapsedMillis = SystemClock.elapsedRealtime() - cronometro.getBase();
                    btnInicioRuta.setBackgroundResource(R.drawable.start_big);
                    //txvEstatusBoton.setText(getResources().getString(R.string.Monitoring_Start_Recorrido));
                    myVibrator.vibrate(100);
                    btnOn = true;
                    cronometro.stop();

                    if(elapsedMillis > 240000){
                        updateRecorrido = new UpdateRecorrido();
                        updateRecorrido.execute();
                    } else {
                        deleteSensado = new DeleteSensado();
                        deleteSensado.execute();
                    }
                }
                break;

            case R.id.recording_button:
                if(!recording)
                {
                    recording = true;
                    recorder.start();
                }
                else
                {
                    recording = false;
                    recorder.stop();
                    initRecorder();
                    prepareRecorder();
                }

                break;
        }

    }

    /**
     * Metodo que se ejecuta para inicializar los sensores del movil
     */
    public void getSensors() {
        sensorManager = (SensorManager) Monitoring.this.getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(this);
        Sensor acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (acelerometro != null) {
            sensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Log.e(getResources().getString(R.string.Monitoring_Get_Sensors_Acelerometro_TAG), getResources().getString(R.string.Monitoring_Get_Sensors_MSG));
        }
        Sensor gyroscopio = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (gyroscopio != null) {
            sensorManager.registerListener(Monitoring.this, gyroscopio, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Log.e(getResources().getString(R.string.Monitoring_Get_Sensors_Giroscopio_TAG), getResources().getString(R.string.Monitoring_Get_Sensors_MSG));
        }
        Sensor magnetometro = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (magnetometro != null) {
            sensorManager.registerListener(this, magnetometro, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Log.e(getResources().getString(R.string.Monitoring_Get_Sensors_Magnetometro_TAG), getResources().getString(R.string.Monitoring_Get_Sensors_MSG));
        }
    }

    /**
     * Clase que se utiliza para gestionar el geoposicionamiento del movil
     */
    public class MyLocationListener implements LocationListener {
        Monitoring mainActivity;
        public Monitoring getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(Monitoring mainActivity) {
            this.mainActivity = mainActivity;
        }

        /**
         * Metodo que se ejecuta cada vez que el GPS detecta un cambio en la ubicacion
         * @param Location loc
         * @return No devuelve algun valor
         * @throws No devuelve ninguna excepción
         */
        @Override
        public void onLocationChanged(Location loc) {
            latitud = loc.getLatitude();
            longitud = loc.getLongitude();
            altitud = loc.getAltitude();
        }

        /**
         * Metodo que se ejecuta cuando el GPS esta desactivado
         * @param String provider
         * @return No devuelve algun valor
         * @throws No devuelve ninguna excepción
         */
        @Override
        public void onProviderDisabled(String provider) {
            mostrarDialogoConfiguracionGPS();
        }

        /**
         * Metodo que se ejecuta cuando el GPS esta activo
         * @param String provider
         * @return No devuelve algun valor
         * @throws No devuelve ninguna excepción
         */
        @Override
        public void onProviderEnabled(String provider) {
        }

        /**
         * Metodo que se ejecuta cada vez que se detecta un cambio en el estatus del proveedor de localizacion
         * @param String provider, int status, Bundle extras
         * @return No devuelve algun valor
         * @throws No devuelve ninguna excepción
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    /**
     * Metodo que se ejecuta al estar deshabilitado el GPS y se le presenta al usuario la opcion de habilitarlo
     */
    private void mostrarDialogoConfiguracionGPS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getResources().getString(R.string.Monitoring_Alert_Dialog_Title));
        alertDialog.setMessage(getResources().getString(R.string.Monitoring_Alert_Dialog_Message));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(getResources().getString(R.string.Monitoring_Alert_Dialog_Positive_Buton),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton(getResources().getString(R.string.Monitoring_Alert_Dialog_Negative_Buton),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    /**
     * Metodo que se ejecuta al mandar la aplicacion a segundo plano
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Metodo que se ejecuta al llamar la aplicacion de la ejecucion en segundo plano
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Metodo que se ejecuta al cerrar la aplicacion
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!btnOn) {
            updateRecorrido = new UpdateRecorrido();
            updateRecorrido.execute();
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermissionRequestCodes.ACCESS_FINE_LOCATION_FINISH);
        }
        else {
            mlocManager.removeUpdates(mlocListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case PermissionRequestCodes.ACCESS_FINE_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    try{
                        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) mlocListener);
                    }
                    catch (SecurityException e){
                        Log.e(LOG_TAG, getString(R.string.Access_fine_location_error));
                    }
                }
                else {
                    Toast.makeText(this, "Se necesita permiso para GPS", Toast.LENGTH_LONG).show();
                }
                break;
            case PermissionRequestCodes.ACCESS_FINE_LOCATION_FINISH:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    try{
                        mlocManager.removeUpdates(mlocListener);
                    }
                    catch (SecurityException e){
                        Log.e(LOG_TAG, getString(R.string.Access_fine_location_error));
                    }
                }
                else {
                    Toast.makeText(this, "Se necesita permiso para GPS", Toast.LENGTH_LONG).show();
                }
                break;
            case PermissionRequestCodes.CAMERA:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    try{
                        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

                        CamcorderProfile cpHigh = CamcorderProfile
                                .get(CamcorderProfile.QUALITY_LOW);
                        recorder.setProfile(cpHigh);
                        Calendar calendarFinal = Calendar.getInstance();

                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH:ss:SS");
                        String formattedDate = df.format(calendarFinal.getTime());
                        recorder.setOutputFile(Environment.getExternalStorageDirectory().getPath() + "/video" + formattedDate + ".mp4");
                    }
                    catch (SecurityException e){
                        Log.e(LOG_TAG, getString(R.string.Request_camera_error));
                    }
                }
                else {
                    Toast.makeText(this, "Se necesita permiso para camara", Toast.LENGTH_LONG).show();
                }
                break;
            case PermissionRequestCodes.RECORD_AUDIO:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                }
                else{
                    Toast.makeText(this, "Se necesita permiso para grabar audio", Toast.LENGTH_LONG).show();
                }
                break;
            case PermissionRequestCodes.STORAGE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    recorder.setPreviewDisplay(holder.getSurface());
                    try {
                        recorder.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                        finish();
                    }
                }
                else{
                    Toast.makeText(this, "Se necesita permiso para camara", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }
}
