package com.plenum.c_video.controllers;

import android.content.Context;
import android.util.Log;

import com.plenum.c_video.R;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.model.Recorrido;
import com.plenum.c_video.services.LecturaDispositivoCtrl;
import com.plenum.c_video.services.RecorridoCtrl;

import java.util.List;

/**
 * Created by jcastro on 17/07/2016.
 */
public class MonitoringController {
    boolean done = false;
    int identificador = 0;
    List<LecturaDispositivo> listaLecturas;
    boolean recorridoDone = false;
    int totalDeLista;
    int quedan;


    /**
     * Método para  la gestion de insercion de un Recorrido en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param recorrido objeto el cual sera registrado
     * @return Devuelve cierto o falso depependiendo de si se registro o no el recorrido
     * @throws Exception si ocurrió un problema al insertar en la base de datos del movil
     */
    public boolean insertRecorrido(Context context, Recorrido recorrido) {
        try {
            if(recorrido != null) {
                RecorridoCtrl recorridoCtrl = new RecorridoCtrl();
                done = recorridoCtrl.insert(context, recorrido);
            }
            else
                Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Recorrido_Empty));
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Insert_Recorrido_Error) + ex);
        }
        return done;
    }

    /**
     * Metodo para  la gestion de insercion de una LecturaDispositivo en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param lecturaDispositivo objeto el cual sera registrado
     * @return Devuelve cierto o falso depependiendo de si se registro o no la lecturaDispositivo
     * @throws Exception si ocurrió un problema al insertar en la base de datos del movil
     */
    public boolean insertLecturaDispositivo(Context context, LecturaDispositivo lecturaDispositivo) {
        try {
            if(lecturaDispositivo != null) {
                LecturaDispositivoCtrl lecturaDispositivoCtrl = new LecturaDispositivoCtrl();
                done = lecturaDispositivoCtrl.insert(context, lecturaDispositivo);
            }
            else
                Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Lectura_Dispositivo_Empty));
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Insert_Lectura_Dispositivo_Error) + ex);
        }
        return done;
    }

    /**
     * Metodo para  la gestion de consulta del ultimo Recorrido en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @return Devuelve un objeto Recorrido
     * @throws Exception si ocurrió un problema al consultar en la base de datos del movil
     */
    public Recorrido retrieveLastRecorrido(Context context) {
        Recorrido recorrido = new Recorrido();
        try {
            RecorridoCtrl recorridoCtrl = new RecorridoCtrl();
            recorrido = recorridoCtrl.retrieveLastRecorrido(context, recorrido);
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Retrieve_Last_Recorrido_Error) + ex);
        }
        return recorrido;
    }

    /**
     * Método para  la gestion de consulta de una lista de LecturasDispositivo en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param recorrido objeto con el cual realizaremos el filtrado de la busqueda
     * @return Devuelve una lista de LecturasDispositivo
     * @throws Exception si ocurrió un problema al consultar en la base de datos del movil
     */
    private List<LecturaDispositivo> retrieveListLecturasDispositivo(Context context, Recorrido recorrido) {
        LecturaDispositivoCtrl lecturaDispositivoCtrl = new LecturaDispositivoCtrl();
        try {
            listaLecturas = lecturaDispositivoCtrl.retrieveListaLecturasDispositivo(context, recorrido);
        } catch (Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Retrieve_Lista_Lectura_Dispositivo_Error) + ex);
        }
        return listaLecturas;
    }

    /**
     * Método para  la gestion de actualizado de un Recorrido en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param recorrido objeto con el cual realizaremos el filtrado de la busqueda
     * @return Devuelve el numero de filas afectadas con la actualizacion
     * @throws Exception si ocurrió un problema al consultar en la base de datos del movil
     */
    public int updateRecorrido(Context context, Recorrido recorrido) {
        try {
            if(recorrido != null) {
                RecorridoCtrl recorridoCtrl = new RecorridoCtrl();
                identificador = recorridoCtrl.update(context, recorrido);
            }
            else
                Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Recorrido_Empty));
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Update_Recorrido_Error) + ex);
        }
        return identificador;
    }

    /**
     * Método para  la gestion de borrado de un Recorrido en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param recorrido objeto con el cual realizaremos el filtrado de la busqueda
     * @return Devuelve cierto o falso depependiendo de si se elimino o no el Registro
     * @throws Exception si ocurrió un problema al consultar en la base de datos del movil
     */
    public boolean deleteRecorrido(Context context, Recorrido recorrido) {
        try {
            quedan = deleteLecturaDispositivo(context, recorrido);
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Delete_Lectura_Dispositivo_Error) + ex);
        }
        if(quedan == 0) {
            try {
                RecorridoCtrl recorridoCtrl = new RecorridoCtrl();
                recorridoDone = recorridoCtrl.delete(context, recorrido);
            }
            catch(Exception ex) {
                Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Delete_Recorrido_Error) + ex);
            }
        }
        return recorridoDone;
    }

    /**
     * Método para  la gestion de borrado de las LecturasDispositivo en la base de datos del movil
     * @param context interfaz global de la informacion de la aplicacion
     * @param recorrido objeto con el cual realizaremos el filtrado de la busqueda
     * @return Devuelve cero en caso de haber eliminado todas las LEcturasDispositivo o el numero de LecturasDispositivo que faltan por eliminar
     * @throws Exception si ocurrio un problema al consultar en la base de datos del movil
     */
    private int deleteLecturaDispositivo(Context context, Recorrido recorrido) { //SE USA
        try {
            listaLecturas = retrieveListLecturasDispositivo(context, recorrido);
            totalDeLista = listaLecturas.size();
        }
        catch(Exception ex) {
            Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Retrieve_Lista_Lectura_Dispositivo_Error) + ex);
        }
        for (LecturaDispositivo lectura : listaLecturas) {
            try{
                LecturaDispositivoCtrl lecturaDispositivoCtrl = new LecturaDispositivoCtrl();
                boolean borradoDone = lecturaDispositivoCtrl.delete(context, lectura);
                if(borradoDone)
                    totalDeLista--;
            }catch(Exception ex) {
                Log.e(context.getResources().getString(R.string.Monitoring_Controller), context.getResources().getString(R.string.Monitoring_Controller_Delete_Lectura_Dispositivo_Error) + ex);
            }
        }
        return totalDeLista;
    }
}
