package com.plenum.c_video.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.R;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.utills.database.DatabaseHandler;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivoDelHlp {

    final String LECTURA_DISPOSITIVO_ID = "lecturaDispositivoId";
    final String TRANSPORTISTA_ID = "transportistaId";
    final String RECORRIDO_ID = "recorridoId";
    final String FECHA_REGISTRO = "fechaRegistro";
    final String ACELEROMETRO_X = "acelerometroX";
    final String ACELEROMETRO_Y = "acelerometroY";
    final String ACELEROMETRO_Z = "acelerometroZ";
    final String MAGNETOMETRO_X = "magnetometroX";
    final String MAGNETOMETRO_Y = "magnetometroY";
    final String MAGNETOMETRO_Z = "magnetometroZ";
    final String GIROSCOPIO_X = "giroscopioX";
    final String GIROSCOPIO_Y = "giroscopioY";
    final String GIROSCOPIO_Z = "giroscopioZ";
    final String LATITUD = "latitud";
    final String LONGITUD = "longitud";
    final String ALTITUD = "altitud";
    final String NIVEL_BATERIA = "nivelBateria";

    final String TABLE_NAME = "tLecturas";

    private boolean lecturaDispositivoDone = false;

    /**
     * Metodo que sirve para eliminar una LecturaDispositivo de la base de datos del movil
     * @param context, LecturaDispositivo lecturaDispositivo
     * @return Devuelve un boolean
     * @throws Excepcion de Error en Base de Datos
     */
    public boolean action(Context context, LecturaDispositivo lecturaDispositivo) {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getWritableDatabase();
        try {
            if (lecturaDispositivo != null) {
                int algo = db.delete(TABLE_NAME, LECTURA_DISPOSITIVO_ID + " = ?", new String[] { String.valueOf(lecturaDispositivo.getLecturaDispositivoId()) });
                if(algo > 0)
                    lecturaDispositivoDone = true;
            }
            lecturaDispositivoDone = true;
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
            lecturaDispositivoDone = false;
        } finally {
            if (db != null)
                db.close();
        }
        return lecturaDispositivoDone;
    }
}
