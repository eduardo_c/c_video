package com.plenum.c_video.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.DateConverter;
import com.plenum.c_video.R;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.utills.database.DatabaseHandler;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivoInsHlp {

    final String TRANSPORTISTA_ID = "transportistaId";
    final String RECORRIDO_ID = "recorridoId";
    final String FECHA_REGISTRO = "fechaRegistro";
    final String ACELEROMETRO_X = "acelerometroX";
    final String ACELEROMETRO_Y = "acelerometroY";
    final String ACELEROMETRO_Z = "acelerometroZ";
    final String MAGNETOMETRO_X = "magnetometroX";
    final String MAGNETOMETRO_Y = "magnetometroY";
    final String MAGNETOMETRO_Z = "magnetometroZ";
    final String GIROSCOPIO_X = "giroscopioX";
    final String GIROSCOPIO_Y = "giroscopioY";
    final String GIROSCOPIO_Z = "giroscopioZ";
    final String LATITUD = "latitud";
    final String LONGITUD = "longitud";
    final String ALTITUD = "altitud";
    final String NIVEL_BATERIA = "nivelBateria";

    final String TABLE_NAME = "tLecturas";

    private boolean lecturaDispositivoDone = false;

    /**
     * Metodo que sirve para insertar una LecturaDispositivo de la base de datos del movil
     * @param Context context, LecturaDispositivo lecturaDispositivo
     * @return Devuelve un boolean
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public boolean action(Context context, LecturaDispositivo lecturaDispositivo) {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getWritableDatabase();

        try {
            if (lecturaDispositivo != null) {
                ContentValues values = new ContentValues();

                if(lecturaDispositivo.getTransportistaId() != null)
                    values.put(TRANSPORTISTA_ID, lecturaDispositivo.getTransportistaId());

                if(lecturaDispositivo.getRecorridoId() != null)
                    values.put(RECORRIDO_ID, lecturaDispositivo.getRecorridoId());

                if(lecturaDispositivo.getFechaRegistro() != null)
                    values.put(FECHA_REGISTRO, new DateConverter().DateToString(lecturaDispositivo.getFechaRegistro()));

                if(lecturaDispositivo.getAcelerometroX() != null)
                    values.put(ACELEROMETRO_X, lecturaDispositivo.getAcelerometroX());

                if(lecturaDispositivo.getAcelerometroY() != null)
                    values.put(ACELEROMETRO_Y, lecturaDispositivo.getAcelerometroY());

                if(lecturaDispositivo.getAcelerometroZ() != null)
                    values.put(ACELEROMETRO_Z, lecturaDispositivo.getAcelerometroZ());

                if(lecturaDispositivo.getMagnetometroX() != null)
                    values.put(MAGNETOMETRO_X, lecturaDispositivo.getMagnetometroX());

                if(lecturaDispositivo.getMagnetometroY() != null)
                    values.put(MAGNETOMETRO_Y, lecturaDispositivo.getAcelerometroY());

                if(lecturaDispositivo.getMagnetometroY() != null)
                    values.put(MAGNETOMETRO_Y, lecturaDispositivo.getMagnetometroY());

                if(lecturaDispositivo.getMagnetometroZ() != null)
                    values.put(MAGNETOMETRO_Z, lecturaDispositivo.getMagnetometroZ());

                if(lecturaDispositivo.getGiroscopioX() != null)
                    values.put(GIROSCOPIO_X, lecturaDispositivo.getGiroscopioX());

                if(lecturaDispositivo.getGiroscopioY() != null)
                    values.put(GIROSCOPIO_Y, lecturaDispositivo.getGiroscopioY());

                if(lecturaDispositivo.getGiroscopioZ() != null)
                    values.put(GIROSCOPIO_Z, lecturaDispositivo.getGiroscopioZ());

                if(lecturaDispositivo.getLatitud() != null)
                    values.put(LATITUD, lecturaDispositivo.getLatitud());

                if(lecturaDispositivo.getLongitud() != null)
                    values.put(LONGITUD, lecturaDispositivo.getLongitud());

                if(lecturaDispositivo.getAltitud() != null)
                    values.put(ALTITUD, lecturaDispositivo.getAltitud());

                if(lecturaDispositivo.getNivelBateria() != null)
                    values.put(NIVEL_BATERIA, lecturaDispositivo.getNivelBateria());

                long algo = db.insert(TABLE_NAME, null, values);
                if(algo > 0)
                    lecturaDispositivoDone = true;
                values.clear();
            }
            lecturaDispositivoDone = true;
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
            lecturaDispositivoDone = false;
        } finally {
            if (db != null)
                db.close();
        }
        return lecturaDispositivoDone;
    }
}
