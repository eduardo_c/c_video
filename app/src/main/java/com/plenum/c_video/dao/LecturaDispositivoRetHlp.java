package com.plenum.c_video.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.DateConverter;
import com.plenum.c_video.R;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.model.Recorrido;
import com.plenum.c_video.utills.database.DatabaseHandler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivoRetHlp {

    final String LECTURA_DISPOSITIVO_ID = "lecturaDispositivoId";
    final String TRANSPORTISTA_ID = "transportistaId";
    final String RECORRIDO_ID = "recorridoId";
    final String FECHA_REGISTRO = "fechaRegistro";
    final String ACELEROMETRO_X = "acelerometroX";
    final String ACELEROMETRO_Y = "acelerometroY";
    final String ACELEROMETRO_Z = "acelerometroZ";
    final String MAGNETOMETRO_X = "magnetometroX";
    final String MAGNETOMETRO_Y = "magnetometroY";
    final String MAGNETOMETRO_Z = "magnetometroZ";
    final String GIROSCOPIO_X = "giroscopioX";
    final String GIROSCOPIO_Y = "giroscopioY";
    final String GIROSCOPIO_Z = "giroscopioZ";
    final String LATITUD = "latitud";
    final String LONGITUD = "longitud";
    final String ALTITUD = "altitud";
    final String NIVEL_BATERIA = "nivelBateria";

    final String TABLE_NAME = "tLecturas";

    Cursor cursor;

    /**
     * Metodo que sirve para cosultar una LecturaDispositivo de la base de datos del movil
     * @param Context context, int lecturaDispositivoId
     * @return Devuelve una LecturaDispositivo
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public LecturaDispositivo action(Context context, int lecturaDispositivoId) throws ParseException {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getReadableDatabase();

        try {
            cursor = db.query(TABLE_NAME, new String[] { LECTURA_DISPOSITIVO_ID,
                            TRANSPORTISTA_ID, RECORRIDO_ID, FECHA_REGISTRO, ACELEROMETRO_X, ACELEROMETRO_Y, ACELEROMETRO_Z,
                            MAGNETOMETRO_X, MAGNETOMETRO_Y, MAGNETOMETRO_Z, GIROSCOPIO_X, GIROSCOPIO_Y, GIROSCOPIO_Z, LATITUD,
                            LONGITUD, ALTITUD, NIVEL_BATERIA }, LECTURA_DISPOSITIVO_ID + "=?",
                    new String[] { String.valueOf(lecturaDispositivoId) }, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return CursorToLecturaDispositivo(cursor);
    }

    /**
     * Metodo que sirve para cosultar una Lista de LecturasDispositivo de la base de datos del movil
     * @param Context context, Recorrido recorrido
     * @return Devuelve una lista de LecturasDispositivo
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public List<LecturaDispositivo> action(Context context, Recorrido recorrido) throws ParseException {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getReadableDatabase();

        List<LecturaDispositivo> listLecturasDispositivo = new ArrayList<LecturaDispositivo>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE recorridoId = " + recorrido.getRecorridoId();

            cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    /** Se añade el elemento a la lista */
                    listLecturasDispositivo.add(CursorToLecturaDispositivo(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return listLecturasDispositivo;
    }

    /**
     * Metodo que sirve para parsear un cursor en una LecturaDispositivo
     * @param Cursor cursor
     * @return Devuelve una LecturasDispositivo
     * @throws Devuelve un ParseException
     */
    private LecturaDispositivo CursorToLecturaDispositivo(Cursor cursor) throws ParseException
    {
        LecturaDispositivo lecturaDispositivo = new LecturaDispositivo();
        if(cursor.getString(0) != null)
            lecturaDispositivo.setLecturaDispositivoId(Integer.parseInt(cursor.getString(0)));
        if(cursor.getString(1) != null)
            lecturaDispositivo.setTransportistaId(Integer.parseInt(cursor.getString(1)));
        if(cursor.getString(2) != null)
            lecturaDispositivo.setRecorridoId(Integer.parseInt(cursor.getString(2)));
        if(cursor.getString(3) != null)
            lecturaDispositivo.setFechaRegistro(new DateConverter().StringToDate(cursor.getString(3)));
        if(cursor.getString(4) != null)
            lecturaDispositivo.setAcelerometroX(cursor.getString(4));
        if(cursor.getString(5) != null)
            lecturaDispositivo.setAcelerometroY(cursor.getString(5));
        if(cursor.getString(6) != null)
            lecturaDispositivo.setAcelerometroZ(cursor.getString(6));
        if(cursor.getString(7) != null)
            lecturaDispositivo.setMagnetometroX(cursor.getString(7));
        if(cursor.getString(8) != null)
            lecturaDispositivo.setMagnetometroY(cursor.getString(8));
        if(cursor.getString(9) != null)
            lecturaDispositivo.setMagnetometroZ(cursor.getString(9));
        if(cursor.getString(10) != null)
            lecturaDispositivo.setGiroscopioX(cursor.getString(10));
        if(cursor.getString(11) != null)
            lecturaDispositivo.setGiroscopioY(cursor.getString(11));
        if(cursor.getString(12) != null)
            lecturaDispositivo.setGiroscopioZ(cursor.getString(12));
        if(cursor.getString(13) != null)
            lecturaDispositivo.setLatitud(Double.parseDouble(cursor.getString(13)));
        if(cursor.getString(14) != null)
            lecturaDispositivo.setLongitud(Double.parseDouble(cursor.getString(14)));
        if(cursor.getString(15) != null)
            lecturaDispositivo.setAltitud(Double.parseDouble(cursor.getString(15)));
        if(cursor.getString(16) != null)
            lecturaDispositivo.setNivelBateria(Integer.parseInt(cursor.getString(16)));

        return lecturaDispositivo;
    }
}
