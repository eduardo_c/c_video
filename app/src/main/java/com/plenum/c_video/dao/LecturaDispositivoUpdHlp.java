package com.plenum.c_video.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.R;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.utills.database.DatabaseHandler;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivoUpdHlp {

    final String LECTURA_DISPOSITIVO_ID = "lecturaDispositivoId";
    final String TRANSPORTISTA_ID = "transportistaId";
    final String RECORRIDO_ID = "recorridoId";
    final String FECHA_REGISTRO = "fechaRegistro";
    final String ACELEROMETRO_X = "acelerometroX";
    final String ACELEROMETRO_Y = "acelerometroY";
    final String ACELEROMETRO_Z = "acelerometroZ";
    final String MAGNETOMETRO_X = "magnetometroX";
    final String MAGNETOMETRO_Y = "magnetometroY";
    final String MAGNETOMETRO_Z = "magnetometroZ";
    final String GIROSCOPIO_X = "giroscopioX";
    final String GIROSCOPIO_Y = "giroscopioY";
    final String GIROSCOPIO_Z = "giroscopioZ";
    final String LATITUD = "latitud";
    final String LONGITUD = "longitud";
    final String ALTITUD = "altitud";
    final String NIVEL_BATERIA = "nivelBateria";

    final String TABLE_NAME = "tLecturas";

    /**
     * Metodo que sirve para actualizar una LecturaDispositivo de la base de datos del movil
     * @param Context context, LecturaDispositivo lecturaDispositivo
     * @return Devuelve un entero
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public int action(Context context, LecturaDispositivo lecturaDispositivo) {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(TRANSPORTISTA_ID, lecturaDispositivo.getTransportistaId());
            values.put(RECORRIDO_ID, lecturaDispositivo.getLecturaDispositivoId());
            values.put(FECHA_REGISTRO, lecturaDispositivo.getFechaRegistro().toString());
            values.put(ACELEROMETRO_X, lecturaDispositivo.getAcelerometroX());
            values.put(ACELEROMETRO_Y, lecturaDispositivo.getAcelerometroY());
            values.put(ACELEROMETRO_Z, lecturaDispositivo.getAcelerometroZ());
            values.put(MAGNETOMETRO_X, lecturaDispositivo.getMagnetometroX());
            values.put(MAGNETOMETRO_Y, lecturaDispositivo.getMagnetometroY());
            values.put(MAGNETOMETRO_Z, lecturaDispositivo.getMagnetometroZ());
            values.put(GIROSCOPIO_X, lecturaDispositivo.getGiroscopioX());
            values.put(GIROSCOPIO_Y, lecturaDispositivo.getGiroscopioY());
            values.put(GIROSCOPIO_Z, lecturaDispositivo.getGiroscopioZ());
            values.put(LATITUD, lecturaDispositivo.getLatitud());
            values.put(LONGITUD, lecturaDispositivo.getLongitud());
            values.put(ALTITUD, lecturaDispositivo.getAltitud());
            values.put(NIVEL_BATERIA, lecturaDispositivo.getNivelBateria());

            /** Se actualiza el elemento */
            int algo = db.update(TABLE_NAME, values, LECTURA_DISPOSITIVO_ID + " = ?",
                    new String[] { String.valueOf(lecturaDispositivo.getLecturaDispositivoId()) });
            if(algo > 0)
            {}
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return 1;
    }
}
