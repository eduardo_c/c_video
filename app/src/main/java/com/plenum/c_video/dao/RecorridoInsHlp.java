package com.plenum.c_video.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.DateConverter;
import com.plenum.c_video.R;
import com.plenum.c_video.model.Recorrido;
import com.plenum.c_video.utills.database.DatabaseHandler;

/**
 * Created by jcastro on 18/07/2016.
 */
public class RecorridoInsHlp {

    final String TRANSPORTISTA_ID = "transportistaId";
    final String FECHA_INICIO = "fechaInicio";
    final String FECHA_FIN = "fechaFin";
    final String ESTATUS_ENVIO = "enviado";

    final String TABLE_NAME = "tRecorridos";

    private boolean recorridoDone = false;

    /**
     * Metodo que sirve para insertar un Recorrido de la base de datos del movil
     * @param Context context, Recorrido recorrido
     * @return Devuelve un boolean
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public boolean action(Context context, Recorrido recorrido)
    {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getWritableDatabase();

        try {
            if (recorrido != null) {
                ContentValues values = new ContentValues();
                if(recorrido.getTransportistaId() != null)
                    values.put(TRANSPORTISTA_ID, recorrido.getTransportistaId());

                if(recorrido.getFechaInicio() != null)
                    values.put(FECHA_INICIO, new DateConverter().DateToString(recorrido.getFechaInicio()));

                if(recorrido.getFechaFin() != null)
                    values.put(FECHA_FIN, new DateConverter().DateToString(recorrido.getFechaFin()));

                if(recorrido.getEnviado() != null)
                    values.put(ESTATUS_ENVIO, recorrido.getEnviado());

                long algo = db.insert(TABLE_NAME, null, values);
                if(algo > 0)
                    recorridoDone = true;
                values.clear();
            }
            recorridoDone = true;
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
            recorridoDone = false;
        } finally {
            if (db != null)
                db.close();
        }
        return recorridoDone;
    }
}
