package com.plenum.c_video.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.DateConverter;
import com.plenum.c_video.R;
import com.plenum.c_video.model.Recorrido;
import com.plenum.c_video.utills.database.DatabaseHandler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcastro on 18/07/2016.
 */
public class RecorridoRetHlp {

    final String RECORRIDO_ID = "recorridoId";
    final String TRANSPORTISTA_ID = "transportistaId";
    final String FECHA_INICIO = "fechaInicio";
    final String FECHA_FIN = "fechaFin";
    final String ESTATUS_ENVIO = "enviado";

    final String TABLE_NAME = "tRecorridos";

    Cursor cursor;

    /**
     * Metodo que sirve para cosultar una LecturaDispositivo de la base de datos del movil
     * @param Context context, int recorridoId
     * @return Devuelve una LecturaDispositivo
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public Recorrido action(Context context, int recorridoId) throws ParseException {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getReadableDatabase();

        try {
            cursor = db.query(TABLE_NAME, new String[] { RECORRIDO_ID,
                            TRANSPORTISTA_ID, FECHA_INICIO, FECHA_FIN, ESTATUS_ENVIO }, RECORRIDO_ID + "=?",
                    new String[] { String.valueOf(recorridoId) }, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return CursorToRecorrido(cursor);
    }

    /**
     * Metodo que sirve para cosultar una Lista de Recorridos de la base de datos del movil
     * @param Context context
     * @return Devuelve una lista de Recorridos
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public List<Recorrido> action(Context context) throws ParseException {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getReadableDatabase();

        List<Recorrido> listRecorrido = new ArrayList<Recorrido>();
        try {
            /** Query para seleccionar todo lo que existe registrado */
            String selectQuery = "SELECT * FROM " + TABLE_NAME;

            Cursor cursor = db.rawQuery(selectQuery, null);

            /** Se recorre cada elemento para añadirlo a una lista */
            if (cursor.moveToFirst()) {
                do {
                    /** Se añade el elemento a la lista */
                    listRecorrido.add(CursorToRecorrido(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return listRecorrido;
    }

    public Recorrido action(Context context, Recorrido recorrido) throws ParseException {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToLast();

        return CursorToRecorrido(cursor);
    }

    /**
     * Metodo que sirve para parsear un cursor en un Recorrido
     * @param Cursor cursor
     * @return Devuelve un Recorrido
     * @throws Devuelve un ParseException
     */
    private Recorrido CursorToRecorrido(Cursor cursor) throws ParseException
    {
        Recorrido recorrido = new Recorrido();
        if(cursor.getString(0) != null)
            recorrido.setRecorridoId(Integer.parseInt(cursor.getString(0)));
        if(cursor.getString(1) != null)
            recorrido.setTransportistaId(Integer.parseInt(cursor.getString(1)));
        if(cursor.getString(2) != null)
            recorrido.setFechaInicio(new DateConverter().StringToDate(cursor.getString(2)));
        if(cursor.getString(3) != null)
            recorrido.setFechaFin(new DateConverter().StringToDate(cursor.getString(3)));
        if(cursor.getString(4) != null)
            recorrido.setEnviado(Boolean.valueOf(cursor.getString(4)));

        return recorrido;
    }
}
