package com.plenum.c_video.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.plenum.c_video.DateConverter;
import com.plenum.c_video.R;
import com.plenum.c_video.model.Recorrido;
import com.plenum.c_video.utills.database.DatabaseHandler;

/**
 * Created by jcastro on 18/07/2016.
 */
public class RecorridoUpdHlp {

    final String RECORRIDO_ID = "recorridoId";
    final String TRANSPORTISTA_ID = "transportistaId";
    final String FECHA_INICIO = "fechaInicio";
    final String FECHA_FIN = "fechaFin";
    final String ESTATUS_ENVIO = "enviado";

    final String TABLE_NAME = "tRecorridos";

    /**
     * Metodo que sirve para actualizar un  Recorrido de la base de datos del movil
     * @param Context context, Recorrido recorrido
     * @return Devuelve un entero
     * @throws Devuelve una excepcion de Error en Base de Datos
     */
    public int action(Context context, Recorrido recorrido) {
        DatabaseHandler createDb = new DatabaseHandler(context);
        SQLiteDatabase db = createDb.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            if(recorrido.getFechaFin() != null)
                values.put(FECHA_FIN, new DateConverter().DateToString(recorrido.getFechaFin()));
            if(recorrido.getEnviado() != null)
                values.put(ESTATUS_ENVIO, recorrido.getEnviado());

            /** Se actualiza el elemento */
            int algo = db.update(TABLE_NAME, values, RECORRIDO_ID + " = ?", new String[] { String.valueOf(recorrido.getRecorridoId()) });
            if(algo > 0)
            {

            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.DAO_Error), e.getMessage());
        } finally {
            if (db != null)
                db.close();
        }
        return 1;
    }
}
