package com.plenum.c_video.model;

import java.util.Date;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivo {
    /**
     * Identificador de la Lectura
     */
    private Integer lecturaDispositivoId;

    /**
     * Identificador del Transportista al que pertenece la Lectura
     */
    private Integer transportistaId;
    /**
     * Identificador del Recorrido al que pertenece la Lectura
     */
    private Integer recorridoId;

    /**
     * Fecha y Hora de Registro de la Lectura
     */
    private Date fechaRegistro;
    /**
     * Valor del Acelerometro en X
     */
    private String acelerometroX;

    /**
     * Valor del Acelerometro en Y
     */
    private String acelerometroY;

    /**
     * Valor del Acelerometro en Z
     */
    private String acelerometroZ;
    /**
     * Valor del Magnetometro en X
     */
    private String magnetometroX;
    /**
     * Valor del Magnetometro en Y
     */
    private String magnetometroY;

    /**
     * Valor del Magnetometro en Z
     */
    private String magnetometroZ;
    /**
     * Valor del Giroscopio en X
     */
    private String giroscopioX;
    /**
     * Valor del Giroscopio en Y
     */
    private String giroscopioY;
    /**
     * Valor del Giroscopio en Z
     */
    private String giroscopioZ;
    /**
     * Valor del GPS Latitud
     */
    private Double latitud;
    /**
     * Valor del GPS Longitud
     */
    private Double longitud;
    /**
     * Valor del GPS Altitud
     */
    private Double altitud;
    /**
     * Valor del Nivel del Bateria del Dispositivo
     */
    private Integer nivelBateria;


    public Integer getLecturaDispositivoId() {
        return lecturaDispositivoId;
    }

    public void setLecturaDispositivoId(Integer lecturaDispositivoId) {
        this.lecturaDispositivoId = lecturaDispositivoId;
    }
    public Integer getTransportistaId() {
        return transportistaId;
    }
    public void setTransportistaId(Integer transportistaId) {
        this.transportistaId = transportistaId;
    }
    public Integer getRecorridoId() {
        return recorridoId;
    }
    public void setRecorridoId(Integer recorridoId) {
        this.recorridoId = recorridoId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }
    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    public String getAcelerometroX() {
        return acelerometroX;
    }

    public void setAcelerometroX(String acelerometroX) {
        this.acelerometroX = acelerometroX;
    }

    public String getMagnetometroY() {
        return magnetometroY;
    }
    public void setMagnetometroY(String magnetometroY) {
        this.magnetometroY = magnetometroY;
    }
    public String getAcelerometroY() {
        return acelerometroY;
    }

    public void setAcelerometroY(String acelerometroY) {
        this.acelerometroY = acelerometroY;
    }
    public String getAcelerometroZ() {
        return acelerometroZ;
    }
    public void setAcelerometroZ(String acelerometroZ) {
        this.acelerometroZ = acelerometroZ;
    }

    public String getMagnetometroX() {
        return magnetometroX;
    }
    public void setMagnetometroX(String magnetometroX) {
        this.magnetometroX = magnetometroX;
    }
    public String getMagnetometroZ() {
        return magnetometroZ;
    }

    public void setMagnetometroZ(String magnetometroZ) {
        this.magnetometroZ = magnetometroZ;
    }
    public String getGiroscopioX() {
        return giroscopioX;
    }
    public void setGiroscopioX(String giroscopioX) {
        this.giroscopioX = giroscopioX;
    }

    public String getGiroscopioY() {
        return giroscopioY;
    }
    public void setGiroscopioY(String giroscopioY) {
        this.giroscopioY = giroscopioY;
    }
    public String getGiroscopioZ() {
        return giroscopioZ;
    }

    public void setGiroscopioZ(String giroscopioZ) {
        this.giroscopioZ = giroscopioZ;
    }
    public Double getLatitud() {
        return latitud;
    }
    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }
    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
    public Double getAltitud() {
        return altitud;
    }

    public void setAltitud(Double altitud) {
        this.altitud = altitud;
    }
    public Integer getNivelBateria() {
        return nivelBateria;
    }
    public void setNivelBateria(Integer nivelBateria) {
        this.nivelBateria = nivelBateria;
    }
}
