package com.plenum.c_video.model;

import java.util.Date;

/**
 * Created by jcastro on 15/07/2016.
 */
public class Recorrido {

    /**
     * Identificador del Recorrido
     */
    private Integer recorridoId;
    /**
     * Identificador del Transportista al que pertenece el Recorrido
     */
    private Integer transportistaId;
    /**
     * Fecha y Hora del Inicio del Recorrido
     */
    private Date FechaInicio;
    /**
     * Fecha y Hora del Fin del Recorrido
     */
    private Date FechaFin;
    /**
     * Estatus del envio del Recorrido
     */
    private Boolean enviado;

    public Integer getRecorridoId() {
        return recorridoId;
    }
    public void setRecorridoId(Integer recorridoId) {
        this.recorridoId = recorridoId;
    }
    public Integer getTransportistaId() {
        return transportistaId;
    }
    public void setTransportistaId(Integer transportistaId) {
        this.transportistaId = transportistaId;
    }

    public Date getFechaInicio() {
        return FechaInicio;
    }
    public void setFechaInicio(Date fechaInicio) {
        FechaInicio = fechaInicio;
    }
    public Date getFechaFin() {
        return FechaFin;
    }
    public void setFechaFin(Date fechaFin) {
        FechaFin = fechaFin;
    }
    public Boolean getEnviado() {
        return enviado;
    }
    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }
}
