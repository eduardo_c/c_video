package com.plenum.c_video.services;

import android.content.Context;
import android.util.Log;

import com.plenum.c_video.R;
import com.plenum.c_video.dao.LecturaDispositivoDelHlp;
import com.plenum.c_video.dao.LecturaDispositivoInsHlp;
import com.plenum.c_video.dao.LecturaDispositivoRetHlp;
import com.plenum.c_video.dao.LecturaDispositivoUpdHlp;
import com.plenum.c_video.model.LecturaDispositivo;
import com.plenum.c_video.model.Recorrido;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcastro on 17/07/2016.
 */
public class LecturaDispositivoCtrl {

    private boolean done = false;

    /**
     * Metodo que sirve gestionar el registro de una LecturaDispositivo en la base de datos del movil
     * @param Context context, LecturaDispositivo lecturaDispositivo
     * @return Devuelve un boolean
     * @throws No dispara ninguna excepcion
     */
    public boolean insert(Context context, LecturaDispositivo lecturaDispositivo) {
        try {
            if (lecturaDispositivo != null) {
                LecturaDispositivoInsHlp lecturaDispositivoInsHlp = new LecturaDispositivoInsHlp();
                done = lecturaDispositivoInsHlp.action(context, lecturaDispositivo);
            } else
                Log.e(context.getResources().getString(R.string.Lectura_Dispositivo_Ctrl_Error), context.getResources().getString(R.string.Lectura_Dispositivo_Empty_Error));
        } catch (Exception ex) {
            Log.e(context.getResources().getString(R.string.Lectura_Dispositivo_Ctrl_Error), context.getResources().getString(R.string.Lectura_Dispositivo_Empty_Error));
        }
        return done;
    }

    /**
     * Metodo que sirve gestionar la consulta de una LecturaDispositivo en la base de datos del movil
     * @param Context context, LecturaDispositivo lecturaDispositivo
     * @return Devuelve una LecturaDispositivo
     * @throws No dispara ninguna excepcion
     */
    public LecturaDispositivo retrieve(Context context, LecturaDispositivo lecturaDispositivo) throws Exception {
        LecturaDispositivo lecturaDispositivoResult = new LecturaDispositivo();
        try {
            if (lecturaDispositivo != null) {
                LecturaDispositivoRetHlp lecturaDispositivoRetHlp = new LecturaDispositivoRetHlp();
                lecturaDispositivoResult = lecturaDispositivoRetHlp.action(context, lecturaDispositivo.getLecturaDispositivoId());
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.Lectura_Dispositivo_Ctrl_Error), context.getResources().getString(R.string.Lectura_Dispositivo_Empty_Error));
        }
        return lecturaDispositivoResult;
    }

    /**
     * Metodo que sirve gestionar la consulta de una lista LecturasDispositivo en la base de datos del movil
     * @param Context context, Recorrido recorrido
     * @return Devuelve una lista de LecturasDispositivo
     * @throws No dispara ninguna excepcion
     */
    public List<LecturaDispositivo> retrieveListaLecturasDispositivo(Context context, Recorrido recorrido) throws Exception{ //USA
        List<LecturaDispositivo> listLecturasDispositivo = new ArrayList<LecturaDispositivo>();
        try {
            LecturaDispositivoRetHlp lecturaDispositivoRetHlp = new LecturaDispositivoRetHlp();
            listLecturasDispositivo = lecturaDispositivoRetHlp.action(context, recorrido);
            return listLecturasDispositivo;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * Metodo que sirve para gestionar la actualizacion de una LecturaDispositivo en la base de datos del movil
     * @param context el context que proveerá acceso a la base de datos
     * @param lecturaDispositivo que usaremos como filtro de búsqueda
     * @return El numero de filas afectadas
     * @throws Exception Exception
     */
    public int update(Context context, LecturaDispositivo lecturaDispositivo) throws Exception {
        int result = 0;
        try {
            if (lecturaDispositivo != null) {
                LecturaDispositivoUpdHlp lecturaDispositivoUpdHlp = new LecturaDispositivoUpdHlp();
                result = lecturaDispositivoUpdHlp.action(context, lecturaDispositivo);
            }
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    /**
     * Metodo para eliminar registros de LecturaDispositivo en la base de datos
     * @param Context context, LecturaDispositivo lecturaDispositivo
     * @return boolean, para saber si se realizo o no el borrado
     * @throws Exception
     */
    public boolean delete(Context context, LecturaDispositivo lecturaDispositivo) throws Exception
    {
        try {
            if (lecturaDispositivo != null) {
                LecturaDispositivoDelHlp lecturaDispositivoDelHlp = new LecturaDispositivoDelHlp();
                done = lecturaDispositivoDelHlp.action(context, lecturaDispositivo);
            } else
                Log.e(context.getResources().getString(R.string.Lectura_Dispositivo_Ctrl_Error), context.getResources().getString(R.string.Lectura_Dispositivo_Empty_Error));
        } catch (Exception e) {
            throw e;
        }
        return done;
    }
}
