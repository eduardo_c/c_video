package com.plenum.c_video.services;

import android.content.Context;
import android.util.Log;

import com.plenum.c_video.R;
import com.plenum.c_video.dao.RecorridoDelHlp;
import com.plenum.c_video.dao.RecorridoInsHlp;
import com.plenum.c_video.dao.RecorridoRetHlp;
import com.plenum.c_video.dao.RecorridoUpdHlp;
import com.plenum.c_video.model.Recorrido;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcastro on 18/07/2016.
 */
public class RecorridoCtrl {

    private boolean done = false;

    /**
     * Metodo que sirve para gestionar el registro de un Recorrido en la base de datos del movil
     * @param Context context
     * @param Recorrido recorrido
     * @return Devuelve cierto o falso dependiendo si se registro o no la informacion
     * @throws Exception si ocurrió un problema al gestionar el registro
     */
    public boolean insert(Context context, Recorrido recorrido) {
        try {
            if (recorrido != null) {
                RecorridoInsHlp recorridoInsHlp = new RecorridoInsHlp();
                done = recorridoInsHlp.action(context, recorrido);
            } else
                Log.e("RecorridoInsHlp.Insert", "El objeto Recorrido no puede ser vacío");
        } catch (Exception ex) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Insert_Error));
        }
        return done;
    }

    /**
     * Metodo que sirve para gestionar la consulta de un Recorrido en la base de datos del movil
     * @param context el context que proveerá acceso a la base de datos
     * @param recorrido que usaremops como filtro de búsqueda
     * @return registro de recorrido completo, null en caso de no encontrarse
     * @throws Exception Exception
     */
    public Recorrido retrieve(Context context, Recorrido recorrido) throws Exception {

        Recorrido recorridoResult = new Recorrido();

        try {
            if (recorrido != null) {
                RecorridoRetHlp recorridoRetHlp = new RecorridoRetHlp();
                recorridoResult = recorridoRetHlp.action(context, recorrido.getRecorridoId());
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Retrieve_Error));
        }
        return recorridoResult;
    }

    /**
     * Metodo que sirve para gestionar la consulta del ultimo Recorrido registrado en la base de datos del movil
     * @param context el context que proveerá acceso a la base de datos
     * @param recorrido que usaremops como filtro de búsqueda
     * @return registro del ultimo Recorrido registrado, null en caso de no encontrarse
     * @throws Exception Exception
     */
    public Recorrido retrieveLastRecorrido(Context context, Recorrido recorrido) throws Exception {
        Recorrido recorridoResult = new Recorrido();
        try {
            if (recorrido != null) {
                RecorridoRetHlp recorridoRetHlp = new RecorridoRetHlp();
                recorridoResult = recorridoRetHlp.action(context, recorrido);
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Retrieve_Error));
        }
        return recorridoResult;
    }

    /**
     * Metodo que sirve para gestionar la consulta del una lista de Recorridos registrado en la base de datos del movil
     * @param context el context que proveerá acceso a la base de datos
     * @return Una lista de registros de Recorridos, null en caso de no encontrarse
     * @throws Exception Exception
     */
    public List<Recorrido> retrieveListaRecorridos(Context context) throws Exception{ //USA
        List<Recorrido> listRecorridos = new ArrayList<Recorrido>();
        try {
            RecorridoRetHlp recorridoRetHlp = new RecorridoRetHlp();
            listRecorridos = recorridoRetHlp.action(context);
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Retrieve_Error));
        }
        return listRecorridos;
    }

    /**
     * Metodo que sirve para gestionar la actualizacion de un Recorrido en la base de datos del movil
     * @param context el context que proveerá acceso a la base de datos
     * @param recorrido que usaremos como filtro de búsqueda
     * @return El numero de filas afectadas
     * @throws Exception Exception
     */
    public int update(Context context, Recorrido recorrido) throws Exception {
        int result = 0;

        try {
            if (recorrido != null)
            {
                RecorridoUpdHlp recorridoUpdHlp = new RecorridoUpdHlp();
                result = recorridoUpdHlp.action(context, recorrido);
            }
        } catch (Exception e) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Update_Error));
        }
        return result;
    }

    /**
     * Metodo para eliminar registros de recorrido en la base de datos
     * @param Context context
     * @param Recorrido recorrido
     * @return boolean, para saber si se realizo o no el borrado
     */
    public boolean delete(Context context, Recorrido recorrido)
    {
        try {
            if (recorrido != null) {
                RecorridoDelHlp recorridoDelHlp = new RecorridoDelHlp();
                done = recorridoDelHlp.action(context, recorrido);
            } else
                Log.e("RecorridoInsHlp.Insert", "El objeto Recorrido no puede ser vacío");
        } catch (Exception ex) {
            Log.e(context.getResources().getString(R.string.Recorrido_Ctrl),
                    context.getResources().getString(R.string.Recorrido_Ctrl_Delete_Error));
        }
        return done;
    }
}
