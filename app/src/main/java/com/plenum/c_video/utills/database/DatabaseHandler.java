package com.plenum.c_video.utills.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jcastro on 17/07/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    /**
     * Nombre de la base de datos
     */
    private static final String DATABASE_NAME = "COMBUSTIBLES.db";

    /**
     * Version de la creacion de la base de datos
     */
    public static final int VERSION = 1;

    /**
     * Constructor
     */
    public DatabaseHandler(Context context){
        /**
         * Método donde le pasamos el contexto, el nombre de la base de datos, el cursor que no lo necesitamos y la version de la base de datos
         */
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * Creación de la base de datos
     */
    @Override
    public void onCreate(SQLiteDatabase db){
        /**
         * Creación de la tabla tRecorrido
         */
        db.execSQL("CREATE TABLE tRecorridos(recorridoId INTEGER PRIMARY KEY, transportistaId INTEGER, FechaInicio DATE, FechaFin DATE, enviado BOOLEAN);");

        /**
         * Creación de la tabla tLecturas
         */
        db.execSQL("CREATE TABLE tLecturas" +
                "(lecturaDispositivoId INTEGER PRIMARY KEY, transportistaId INTEGER, recorridoId INT, fechaRegistro DATE, " +
                "acelerometroX VARCHAR, acelerometroY VARCHAR, acelerometroZ VARCHAR, " +
                "magnetometroX VARCHAR, magnetometroY VARCHAR, magnetometroZ VARCHAR, " +
                "giroscopioX VARCHAR, giroscopioY VARCHAR, giroscopioZ VARCHAR, " +
                "latitud DOUBLE, longitud DOUBLE, altitud DOUBLE, nivelBateria DOUBLE, " +
                "FOREIGN KEY (recorridoId) REFERENCES tRecorrido(recorridoId));");
    }

    /**
     * Método donde se actualizará la base de datos
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
